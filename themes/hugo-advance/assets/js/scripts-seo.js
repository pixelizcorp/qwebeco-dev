<script type='application/ld+json'> 
{
  '@context': 'http://www.schema.org',
  '@type': 'Organization',
  'name': 'QWEBECO | Agence de Qualité WEB ECO-responsable',
  'url': 'https://www.qweb.eco',
  'logo': 'https://www.qweb.eco/logo_qwebeco.svg',
  'image': 'https://www.qweb.eco/image_qwebeco.svg',
  'description': 'Qweb.eco est une agence de marketing digital qui vous rend performant sur internet. Vous êtes accompagné de A a Z par des experts passionnés du numérique et vous prenez les bonnes décisions pour développer votre activité commerciale en ligne. ',
  'address': {
    '@type': 'PostalAddress',
    'streetAddress': '14C Faubourg Reclus',
    'addressLocality': 'Chambéry',
    'addressRegion': 'Auvergne Rhône-Alpes',
    'postalCode': '73000',
    'addressCountry': 'FRANCE'
  },
  'geo': {
    '@type': 'GeoCoordinates',
    'latitude': '5.922007',
    'longitude': '45.568905'
  },
  'openingHours': 'Mo, Tu 08:00-18:00',
  'contactPoint': {
    '@type': 'ContactPoint',
    'telephone': '+33783985948',
    'contactType': 'Service client'
  }
}
 </script>
 
  <script type='application/ld+json'> 
{
  '@context': 'http://www.schema.org',
  '@type': 'WebSite',
  'name': 'QWEBECO| Agence de Qualité WEB ECO-responsable',
  'alternateName': 'QWEB.ECO',
  'url': 'https://qweb.eco'
}
 </script>
