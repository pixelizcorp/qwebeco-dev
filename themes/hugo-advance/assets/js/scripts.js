
// Scroll pour les shortcodes

const headerHeight = document.getElementById('header').offsetHeight;
const allBoxText = document.querySelectorAll('.box-text');

// Je checke la taille du Array pour pouvoir appliquer du style au premier et dernier élément
if(allBoxText.length != ''){
  const boxText = Array.from(allBoxText);
  const taille = boxText.length;
  boxText[0].style.marginTop = "8em";
  boxText[taille-1].style.marginBottom = "8em"; // -1 car on commence à 0 ^^


boxText.forEach(box => {
    let cible = box.id;
    let link = box.querySelector('.icon-next-section');
    let pos = link.getBoundingClientRect().top + window.scrollY;
    link.href = "#" + cible;

    link.addEventListener('click', function(e){
        e.preventDefault();
        window.scroll(0 , pos + headerHeight); // (X , Y)
    });
});

}

// Accordéons

const acc = document.getElementsByClassName("accordion");

let i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {

    this.classList.toggle("active");

    const panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}

// Slider Questions

